﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEditor;

public class PrizeBehavior : MonoBehaviour
{
    public Transform model;

    public float rotSpeed;

    // Start is called before the first frame update
    void Start()
    {
        model.DOMoveY(2, 2f).SetLoops(-1, LoopType.Yoyo);
    }

    private void Update()
    {
        model.RotateAround(model.transform.position, Vector3.one, rotSpeed * Time.deltaTime);
    }
}
