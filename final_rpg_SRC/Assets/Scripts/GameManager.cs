﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        OnWarmup,
        OnStart,
        OnPlay,
        OnWin,
        OnLose
    }

    public static GameManager instance;

    public List<Command> commandsList;

    public PlayerBehavior playerBehavior;

    private Coroutine _commandsRoutine;

    private bool _isExecuting;

    private GameState _currentState;

    [Header("Level")]
    public LevelData[] levels;
    public int level;

    private int prizes;
    private int levelPrizes;
    private LevelData _currentLevelData;
    private Vector3 _startPos;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        commandsList = new List<Command>();
    }

    private void Start()
    {
        ChangeState(GameState.OnWarmup);
    }

    #region States related functions
    public void ChangeState(GameState state)
    {
        _currentState = state;
        Debug.Log(">>>GameManager::ChangeState " + _currentState);
        switch (_currentState)
        {
            case GameState.OnWarmup:
                StartCoroutine(OnWarmupState());
                break;
            case GameState.OnStart:
                StartCoroutine(OnStartState());
                break;
            case GameState.OnPlay:
                StartCoroutine(OnPlayState());
                break;
            case GameState.OnWin:
                StartCoroutine(OnWinState());
                break;
            case GameState.OnLose:
                StartCoroutine(OnLoseState());
                break;
            default:
                break;
        }
    }

    private IEnumerator OnWarmupState()
    {
        for (int i = 0, length = levels.Length; i < length; i++)
        {
            LevelData tmp = levels[i];
            if (tmp.level == level)
            {
                _currentLevelData = tmp;
                break;
            }
        }

        _currentLevelData.gameObject.SetActive(true);
        _startPos = _currentLevelData.startPoint.position;
        _startPos.y = 0;
        playerBehavior.ResetPlayer(_startPos);
        //playerBehavior.transform.position = _startPos;

        prizes = 0;
        levelPrizes = _currentLevelData.prizes.Length;

        ActivityPanelManager.instance.EmptyActionList();
        commandsList.Clear();

        TopPanelManager.instance.SetLevelText(level);

        ChangeState(GameState.OnStart);
        yield return null;
    }

    private IEnumerator OnStartState()
    {
        yield return new WaitForSeconds(1.5f);
        LoadingPopupManager.instance.Hide();
        ChangeState(GameState.OnPlay);
        yield return null;
    }

    private IEnumerator OnPlayState()
    {
        yield return null;
    }

    private IEnumerator OnWinState()
    {
        AudioManager.instance.PlaySound("win");
        WinPopupManager.instance.Show(
            () =>
            {
                _currentLevelData.gameObject.SetActive(false);
                level++;
                LoadingPopupManager.instance.Show();
                if (level <= 10)
                {
                    ChangeState(GameState.OnWarmup);
                }
                else
                {
                    FinishPopupManager.instance.Show();
                }
            });
        yield return null;
    }

    private IEnumerator OnLoseState()
    {
        AudioManager.instance.PlaySound("lose");
        LosePopupManager.instance.Show(
            () =>
            {
                playerBehavior.ResetPlayer(_startPos);
            });
        yield return null;
    }
    #endregion

    /// <summary>
    /// Add command to the list
    /// </summary>
    /// <param name="command"></param>
    /// <param name="type"></param>
    public void AddCommand(Command command, CommandType type)
    {
        ActivityPanelManager.instance.AddCommandToActivity(type);
        commandsList.Add(command);
    }

    /// <summary>
    /// Execute all commands in the list
    /// </summary>
    public void ExecuteCommands()
    {
        Debug.Log(">>>GameManager::ExecuteCommands");
        if (!_isExecuting)
        {
            CommandPanelManager.instance.BlockerOn();
            playerBehavior.ResetPlayer(_startPos);
            _isExecuting = true;
            _commandsRoutine = StartCoroutine(ExecuteRoutine());
        }
    }

    /// <summary>
    /// Coroutine to handle commands execution
    /// </summary>
    /// <returns></returns>
    IEnumerator ExecuteRoutine()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0, length = commandsList.Count; i < length; i++)
        {
            commandsList[i].Execute();
            yield return new WaitForSeconds(1f);
        }
        ActivityPanelManager.instance.EnableInteraction();
        CommandPanelManager.instance.BlockerOff();
        _isExecuting = false;
    }

    /// <summary>
    /// Stop execution
    /// </summary>
    public void StopExecution()
    {
        if (_commandsRoutine != null)
            StopCoroutine(_commandsRoutine);
        ActivityPanelManager.instance.EnableInteraction();
        CommandPanelManager.instance.BlockerOff();
        _isExecuting = false;
    }

    /// <summary>
    /// Displace current command to previous one.
    /// </summary>
    /// <param name="curIndex"></param>
    public void DisplaceCommandUp(int curIndex)
    {
        if (curIndex - 1 >= 0)
        {
            SwapCommand(curIndex, curIndex - 1);
        }
    }

    /// <summary>
    /// Displace current command to next one.
    /// </summary>
    /// <param name="curIndex"></param>
    public void DisplaceCommandDown(int curIndex)
    {
        if (curIndex + 1 < commandsList.Count)
        {
            SwapCommand(curIndex, curIndex + 1);
        }
    }

    /// <summary>
    /// Swap command in the list.
    /// </summary>
    /// <param name="curIndex"></param>
    /// <param name="tmpIndex"></param>
    private void SwapCommand(int curIndex, int tmpIndex)
    {
        Command curCommand = commandsList[curIndex];
        Command tmpCommand = commandsList[tmpIndex];

        commandsList[curIndex] = tmpCommand;
        commandsList[tmpIndex] = curCommand;
    }

    /// <summary>
    /// Delete a specific command
    /// </summary>
    /// <param name="id"></param>
    public void DeleteCommand(int id)
    {
        commandsList.RemoveAt(id);
    }

    /// <summary>
    /// Counting collectibles
    /// </summary>
    public void AddCollectible()
    {
        prizes++;
        if (prizes == levelPrizes)
        {
            ChangeState(GameState.OnWin);
        }
    }

    /// <summary>
    /// Triggering lose state
    /// </summary>
    public void TriggerLoseState()
    {
        ChangeState(GameState.OnLose);
    }
}
