﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
    public int level;

    public Transform startPoint;    

    public PrizeBehavior[] prizes;
}
