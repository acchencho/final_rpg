﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveCommand : Command
{
    PlayerBehavior _entity;    

    public MoveCommand(PlayerBehavior entity) : base()
    {
        _entity = entity;
    }

    public override void Execute()
    {
        _entity.Move();
        //base.Execute();
    }
}
