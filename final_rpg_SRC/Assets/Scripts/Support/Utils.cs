﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils
{    
}

public enum CommandType
{
    Move,
    RotateLeft,
    RotateRight,
    Conditional,
    ForLoop,
    WhileLoop,
    Attack,
    None
}

public enum Direction
{
    Front = 0,
    Back = 2, 
    Right = 1,
    Left = 3
}