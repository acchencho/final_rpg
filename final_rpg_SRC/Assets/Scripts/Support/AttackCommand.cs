﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCommand : Command
{
    PlayerBehavior _entity;

    public AttackCommand(PlayerBehavior entity) : base()
    {
        _entity = entity;
    }

    public override void Execute()
    {
        _entity.Attack();
    }
}
