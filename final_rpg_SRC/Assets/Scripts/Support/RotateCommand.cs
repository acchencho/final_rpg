﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCommand : Command
{
    PlayerBehavior _entity;
    bool _isRight;
    
    public RotateCommand(PlayerBehavior entity, bool isRight) : base()
    {
        _entity = entity;
        _isRight = isRight;
    }

    public override void Execute()
    {
        _entity.Rotate(_isRight);
    }
}
