﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;

    public SoundData[] soundDatas;

    public static AudioManager instance;

    public Dictionary<string, AudioClip> soundPool;

    void Awake()
    {
        instance = this;

        soundPool = new Dictionary<string, AudioClip>();

        for (int i = 0, length = soundDatas.Length; i < length; i++)
        {
            SoundData data = soundDatas[i];
            soundPool.Add(data.key, data.clip);
        }
    }

    public void PlaySound(string key)
    {
        if (soundPool.ContainsKey(key))
        {
            audioSource.clip = soundPool[key];
            audioSource.Play();
        }
    }
}

[System.Serializable]
public class SoundData
{
    public string key;
    public AudioClip clip;
}