﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityPanelManager : MonoBehaviour
{
    public static ActivityPanelManager instance;

    public GameObject contents;

    public GameObject btnSimpleActionPrefab;
    public GameObject btnForActionPrefab;

    public int itemsHidden;

    public GameObject actionContentContainer;

    public GameObject blockerContainer;

    [SerializeField]
    private Button _deleteBtn;

    private List<GameObject> _actionsList;

    private void Awake()
    {
        instance = this;
        _actionsList = new List<GameObject>();
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnExecutePressed()
    {
        BlockInteraction();
        GameManager.instance.ExecuteCommands();
    }

    public void AddCommandToActivity(CommandType type)
    {
        GameObject obj = null;
        switch (type)
        {
            case CommandType.Move:
            case CommandType.RotateLeft:
            case CommandType.RotateRight:
            case CommandType.Attack:
                obj = GameObject.Instantiate(btnSimpleActionPrefab, Vector3.zero, Quaternion.identity, actionContentContainer.transform);
                break;
            case CommandType.Conditional:
                break;
            case CommandType.ForLoop:
                break;
            case CommandType.WhileLoop:
                break;
            case CommandType.None:
                break;
            default:
                break;
        }

        if (obj != null)
        {
            _actionsList.Add(obj);
            obj.SetActive(true);

            ButtonActionActivity btnActionActivity = obj.GetComponent<ButtonActionActivity>();
            if (btnActionActivity != null)
            {
                btnActionActivity.Init(type);
            }
        }
    }

    public int ContentSize()
    {
        return actionContentContainer.transform.childCount;
    }

    public void BlockInteraction()
    {
        blockerContainer.SetActive(true);
        _deleteBtn.interactable = false;
    }

    public void EnableInteraction()
    {
        blockerContainer.SetActive(false);
        _deleteBtn.interactable = true;
    }

    public void OnBtnEmptyPressed()
    {
        EmptyActionList();

        GameManager.instance.commandsList.Clear();

        Debug.Log(">>>Action List " + _actionsList.Count + " GameManager Comand List " + GameManager.instance.commandsList.Count);
    }

    public void EmptyActionList()
    {
        for (int i = 0, length = _actionsList.Count; i < length; i++)
        {
            Destroy(_actionsList[i]);
        }
    }
}