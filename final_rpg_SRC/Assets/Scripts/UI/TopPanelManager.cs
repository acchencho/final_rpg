﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TopPanelManager : MonoBehaviour
{
    public static TopPanelManager instance;

    public GameObject contents;

    public TextMeshProUGUI levelText;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void SetLevelText(int level)
    {
        levelText.text = level.ToString();
    }

    public void OnBtnOptionsPressed()
    {
        OptionPopupManager.instance.Show();
    }
}
