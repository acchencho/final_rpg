﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCommand : MonoBehaviour
{
    private Command _command;

    public CommandType commandType;

    public void OnBtnCommandPressed()
    {
        switch (commandType)
        {
            case CommandType.Move:
                _command = new MoveCommand(GameManager.instance.playerBehavior);
                break;
            case CommandType.RotateLeft:
                _command = new RotateCommand(GameManager.instance.playerBehavior, false);
                break;
            case CommandType.RotateRight:
                _command = new RotateCommand(GameManager.instance.playerBehavior, true);
                break;
            case CommandType.Conditional:
                break;
            case CommandType.ForLoop:
                break;
            case CommandType.WhileLoop:
                break;
            case CommandType.Attack:
                _command = new AttackCommand(GameManager.instance.playerBehavior);
                break;
            default:
                break;
        }

        GameManager.instance.AddCommand(_command, commandType);
        //Debug.Log(">>>" + commandType + " Added");
    }
}
