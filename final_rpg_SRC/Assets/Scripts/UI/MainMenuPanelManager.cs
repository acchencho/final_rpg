﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MainMenuPanelManager : MonoBehaviour
{
    public static MainMenuPanelManager instance;

    public GameObject contents;

    void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnStartPressed()
    {
        SceneLoadManager.instance.LoadScene("LoadingScene", "Game");
    }

    public void OnBtnCreditsPressed()
    {
        Hide();
        CreditsPanelManager.instance.Show();
    }

    public void OnBtnQuitPressed()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;

#else
        Application.Quit();
#endif
    }

}
