﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPanelManager : MonoBehaviour
{
    public static CommandPanelManager instance;

    public GameObject contents;

    [SerializeField]
    private GameObject _blockerContainer;

    private void Awake()
    {
        instance = this;
        BlockerOff();
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void BlockerOn()
    {
        _blockerContainer.SetActive(true);
    }

    public void BlockerOff()
    {
        _blockerContainer.SetActive(false);
    }
}
