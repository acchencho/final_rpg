﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsPanelManager : MonoBehaviour
{
    public static CreditsPanelManager instance;

    public GameObject contents;

    void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnBackPressed()
    {
        Hide();
        MainMenuPanelManager.instance.Show();
    }
}
