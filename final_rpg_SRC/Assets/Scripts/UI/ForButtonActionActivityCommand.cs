﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ForButtonActionActivityCommand : ButtonActionActivity
{
    public TextMeshProUGUI repText;
    public int repetition;

    public void OnBtnPlusPressed()
    {
        if (repetition < 1000)
        {
            repetition++;
            repText.text = repetition.ToString();
        }
    }

    public void OnBtnMinusPressed()
    {
        if (repetition > 0)
        {
            repetition--;
            repText.text = repetition.ToString();
        }
    }
}
