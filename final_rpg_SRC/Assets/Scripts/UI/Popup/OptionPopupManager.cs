﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionPopupManager : MonoBehaviour
{
    public static OptionPopupManager instance;

    public GameObject contents;

    private void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnClosePressed()
    {
        Hide();
    }

    public void OnBtnMainMenuPressed()
    {
        SceneLoadManager.instance.LoadScene("LoadingScene", "MainScene");
    }

    public void OnBtnQuitPressed()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#else
        Application.Quit();
#endif
    }
}
