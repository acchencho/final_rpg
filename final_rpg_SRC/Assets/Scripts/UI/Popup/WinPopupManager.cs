﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinPopupManager : MonoBehaviour
{
    public static WinPopupManager instance;

    public GameObject contents;

    private Action _callback;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public void Show(Action callback = null)
    {
        _callback = callback;
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnAnyPressed()
    {
        Hide();
        if (_callback != null)
            _callback.Invoke();
    }
}
