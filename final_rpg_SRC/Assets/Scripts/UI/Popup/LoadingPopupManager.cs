﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPopupManager : MonoBehaviour
{
    public static LoadingPopupManager instance;

    public GameObject contents;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }
}
