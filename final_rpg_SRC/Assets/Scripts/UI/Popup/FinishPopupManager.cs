﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishPopupManager : MonoBehaviour
{
    public static FinishPopupManager instance;

    public GameObject contents;

    private void Awake()
    {
        instance = this;
    }

    public void Show()
    {
        contents.SetActive(true);
    }

    public void Hide()
    {
        contents.SetActive(false);
    }

    public void OnBtnMainMenuPressed()
    {
        SceneLoadManager.instance.LoadScene("LoadingScene", "Main");
    }

    public void OnBtnQuitPressed()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#else
        Application.Quit();
#endif
    }
}
