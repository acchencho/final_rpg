﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonActionActivity : MonoBehaviour
{
    public int actionID;
    public int subActionID;

    private bool _isOption;

    public TextMeshProUGUI actionTxt;

    [SerializeField]
    private CommandType _commandType;

    public GameObject btnUpContainer, btnDownContainer, btnDeleteContainer;

    public virtual void Init(CommandType type)
    {
        _commandType = type;

        switch (_commandType)
        {
            case CommandType.Move:
                actionTxt.text = "MOVE";
                break;
            case CommandType.RotateLeft:
                actionTxt.text = "ROT LEFT";
                break;
            case CommandType.RotateRight:
                actionTxt.text = "ROT RIGHT";
                break;
            case CommandType.Conditional:
                break;
            case CommandType.ForLoop:
                break;
            case CommandType.WhileLoop:
                break;
            case CommandType.Attack:
                actionTxt.text = "ATTACK";
                break;
            case CommandType.None:
                break;
            default:
                break;
        }

        _isOption = true;
        actionID = transform.GetSiblingIndex();
        // TODO :: set is option behavior
    }

    public void OnBtnUpPressed()
    {
        actionID = transform.GetSiblingIndex();
        if (actionID > ActivityPanelManager.instance.itemsHidden)
        {
            Debug.Log(">>>Can go up");
            actionID--;
            transform.SetSiblingIndex(actionID);
            GameManager.instance.DisplaceCommandUp(actionID - 1);
        }
    }

    public void OnBtnDownPressed()
    {
        actionID = transform.GetSiblingIndex();
        if (actionID < ActivityPanelManager.instance.ContentSize() - 1)
        {
            Debug.Log(">>>Can go down");
            actionID++;
            transform.SetSiblingIndex(actionID);
            GameManager.instance.DisplaceCommandDown(actionID - 3);
        }
    }

    public void OnBtnDeletePressed()
    {
        int value = transform.GetSiblingIndex();
        GameManager.instance.DeleteCommand(value - 2);        
        Destroy(this.gameObject);
    }
}
