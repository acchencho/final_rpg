﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// LoadingPanelManager: panel to handle the loading UI 
/// </summary>
public class LoadingPanelManager : MonoBehaviour
{
    public static LoadingPanelManager instance;

    public Slider loadingSlider;
    public GameObject anyLabelCont;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        anyLabelCont.SetActive(false);
    }

    public void UpdateSlider(float value)
    {
        loadingSlider.value = value;
    }

    public void SetAnyLableActive()
    {
        anyLabelCont.SetActive(true);
    }
}
