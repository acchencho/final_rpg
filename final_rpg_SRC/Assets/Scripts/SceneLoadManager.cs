﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SceneLoadManager : MonoBehaviour
{
    public static SceneLoadManager instance;

    public string NextScene;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadScene(string sceneName, string nextScene)
    {
        NextScene = nextScene;
        SceneManager.LoadScene(sceneName);
    }

    public void LoadSceneAsync()
    {
        if (!string.IsNullOrEmpty(NextScene))
        {
            StartCoroutine(LoadSceneAsyncRoutine());
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    IEnumerator LoadSceneAsyncRoutine()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(NextScene);
        async.allowSceneActivation = false;
        float value = 0;
        while (!async.isDone)
        {
            value = async.progress / 0.9f;
            if (LoadingPanelManager.instance != null) LoadingPanelManager.instance.UpdateSlider(value);

            if (async.progress >= 0.9f)
            {
                if (LoadingPanelManager.instance != null) LoadingPanelManager.instance.SetAnyLableActive();

                if (Keyboard.current.anyKey.isPressed)
                {
                    async.allowSceneActivation = true;
                }
            }
            yield return null;
        }
    }
}
