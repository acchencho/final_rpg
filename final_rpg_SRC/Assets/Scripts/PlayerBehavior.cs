﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerBehavior : MonoBehaviour
{
    public float speed = 1;
    public float smoothSpeed = 1;

    [SerializeField]
    private Animator _anim;
    private Rigidbody _rb;
    private bool _isJump;
    Vector3 _move;
    Vector3 smoothDirection;

    [SerializeField]
    private Direction _direction;
    private Tween _characterTween;
    private Vector3 _prevPos;

    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (_move.magnitude > 0)
        //{
        //    Vector3 direction = _move.normalized;
        //    _rb.MovePosition(transform.position + direction * Time.deltaTime * speed);

        //    smoothDirection = Vector3.Lerp(smoothDirection, direction, Time.deltaTime * smoothSpeed);
        //    Quaternion rot = Quaternion.LookRotation(smoothDirection);
        //    _rb.MoveRotation(rot);
        //}

        //Attack();
    }

    public void Move(InputAction.CallbackContext callback)
    {
        //Vector2 moveVec = callback.ReadValue<Vector2>();

        //_move = new Vector3(moveVec.x, 0, moveVec.y);

        //if (_move.magnitude > 0)
        //{
        //    _anim.SetBool("Walking", true);
        //}
        //else
        //{
        //    _anim.SetBool("Walking", false);
        //}
    }

    public void Jump(InputAction.CallbackContext callback)
    {
        //_isJump = callback.performed;
    }

    public void Fire(InputAction.CallbackContext callback)
    {
        //if (callback.started)
        //{
        //    attackTime = 0;
        //    actionCount = 0;
        //    attackTrigger = false;
        //}
        //isAttacking = callback.performed;
    }

    //int actionCount;
    //bool isAttacking;
    //bool attackTrigger;
    //float attackTime = 0;
    //float attackLoaded = 1.5f;
    //private void Attack()
    //{
    //    if (isAttacking)
    //    {
    //        //actionCount++;
    //        if (!attackTrigger)
    //        {
    //            attackTrigger = true;
    //            _anim.SetTrigger("Attack");
    //            _anim.SetBool("AttackCombo", true);
    //        }
    //        //if (actionCount == 3)
    //        //{
    //        //    Debug.Log("Trigger attack");
    //        //    _anim.SetBool("AttackCombo", true);
    //        //}

    //        attackTime += Time.deltaTime;
    //        if (attackTime >= attackLoaded)
    //        {
    //            _anim.SetBool("AttackCombo", false);
    //            actionCount = 0;
    //            attackTime = 0;
    //            attackTrigger = false;
    //        }
    //    }
    //    else
    //    {
    //        _anim.SetBool("AttackCombo", false);
    //    }
    //}

    public void Move()
    {
        Vector2 direction = Vector2.zero;
        switch (_direction)
        {
            case Direction.Front:
                direction = Vector2.up;
                break;
            case Direction.Back:
                direction = -Vector2.up;
                break;
            case Direction.Right:
                direction = Vector2.right;
                break;
            case Direction.Left:
                direction = Vector2.left;
                break;
            default:
                break;
        }
        Vector3 pos = transform.position;
        pos.x += (direction.x * 2);
        pos.z += (direction.y * 2);
        _anim.SetBool("Walking", true);
        AudioManager.instance.PlaySound("step");
        _characterTween = transform.DOMove(pos, 1f).OnComplete(() => { _anim.SetBool("Walking", false); });
    }

    public void Rotate(bool isRight)
    {
        Vector3 angleVec = transform.rotation.eulerAngles;
        if (isRight)
        {
            angleVec.y += 90f;
            _direction++;
            if ((int)_direction > 3)
            {
                _direction = 0;
            }
        }
        else
        {
            angleVec.y -= 90f;
            _direction--;
            if ((int)_direction < 0)
            {
                _direction = (Direction)3;
            }
        }
        _characterTween = transform.DORotate(angleVec, 1f);
    }

    public void Attack()
    {
        _anim.SetTrigger("Attack");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("TileNode"))
        {
            _prevPos = other.transform.position;
        }
        else if (other.CompareTag("TileBorder"))
        {
            GameManager.instance.StopExecution();
            if (_characterTween != null)
            {
                _anim.SetBool("Walking", false);
                transform.position = _prevPos;
                _characterTween.Kill();
            }
            GameManager.instance.TriggerLoseState();
        }
        else if (other.CompareTag("Collectible"))
        {
            Debug.Log(">>>Collect the stuff");
            GameManager.instance.AddCollectible();
        }
    }

    public void ResetPlayer(Vector3 position, Direction direction = Direction.Right)
    {
        _direction = direction;
        Quaternion rotation = Quaternion.Euler(0, 90f, 0);
        Debug.Log(">>>Reset Player");
        transform.SetPositionAndRotation(position, rotation);
    }
}
