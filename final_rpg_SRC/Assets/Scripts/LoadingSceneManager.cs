﻿using UnityEngine;

/// <summary>
/// Simple class to handle the loading scene.
/// </summary>
public class LoadingSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {        
        SceneLoadManager.instance.LoadSceneAsync();
    }
}
